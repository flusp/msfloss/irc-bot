---
swagger: "2.0"
info:
  description: IRC Robot API
  version: 1.0.0
  title: IRC Robot
host: virtserver.swaggerhub.com
basePath: /sergiolive/msfloss-irc/1.0.0
tags:
- name: messages
  description: Fetch IRC messages.
- name: register
  description: Register new server-channel pair.
- name: unregister
  description: Unregister monitored channel.
- name: registered
  description: Fetch registered channels.
- name: resources
  description: Pull this doc as JSON.
- name: count
  description: Count number of messages that pass parameter criteria.
- name: activity
  description: Fetch number of messages by date or time.

schemes:
- https
- http
paths:
  /messages:
    get:
      tags:
      - messages
      summary: Get IRC messages from monitored channels.
      operationId: messages
      produces:
      - application/json
      parameters:
      - name: nick
        in: query
        description: nickname
        required: false
        type: string
        example: 'msfoss'
      - name: channel
        in: query
        description: channel
        required: false
        type: string
        example: '#msfloss'
      - name: server
        in: query
        description: server
        example: 'irc.freenode.net'
      - name: from
        in: query
        description: date from in YYYYMMDD format
        required: false
        type: string
        example: '2019-01-14'
      - name: to
        in: query
        description: end date of interval to search in YYYYMMDD format
        required: false
        type: string
        example: '2019-09-23'
      - name: time
        in: query
        description: exact timestamp
        required: false
        type: string
        example: '2019-01-14 08:00:00 UTC'
      - name: message
        in: query
        description: exact message string
        example: 'msfloss: what a great bot!'
      - name: discretize
        in: query
        description: whether to discretize timestamps by date only
        allowEmptyValue: true
      responses:
        200:
          description: successful operation
          schema:
            type: array
            items:
              $ref: '#/definitions/Message'
  /registered:
    get:
      tags:
      - registered
      summary: Get registered channel
      operationId: registered
      produces:
      - application/json
      parameters:
        - name: server
          in: query
          description: match server name
          example: 'irc.freenode.net'
        - name: channel
          in: query
          description: match channel name
          example: '#msfloss'
      responses:
        200:
          description: successful operation
          schema:
            type: string
            example: '{"irc.freenode.net"=>["#flusp", "#msfloss"], "irc.oftc.net"=>["#kernelnewbies"]}'
  /count:
    get:
      tags:
      - count
      summary: Count number of messages that match parameters
      operationId: count
      produces:
      - application/json
      parameters:
      - name: nick
        in: query
        description: nickname
        required: false
        type: string
        example: 'msfoss'
      - name: channel
        in: query
        description: channel
        required: false
        type: string
        example: '#msfloss'
      - name: server
        in: query
        description: server
        example: 'irc.freenode.net'
      - name: from
        in: query
        description: date from in YYYYMMDD format
        required: false
        type: string
        example: '2019-01-14'
      - name: to
        in: query
        description: end date of interval to search in YYYYMMDD format
        required: false
        type: string
        example: '2019-09-23'
      - name: time
        in: query
        description: exact timestamp
        required: false
        type: string
        example: '2019-01-14 08:00:00 UTC'
      - name: message
        in: query
        description: exact message string
        example: 'msfloss: what a great bot!'
      responses:
        200:
          description: successfull operation
          schema:
            type: int32

  /activity:
    get:
      tags:
      - activity
      summary: Fetch number of messages by date or time
      operationId: activity
      produces:
        - application/json
      parameters:
      - name: nick
        in: query
        description: nickname
        required: false
        type: string
        example: 'msfoss'
      - name: channel
        in: query
        description: channel
        required: false
        type: string
        example: '#msfloss'
      - name: server
        in: query
        description: server
        example: 'irc.freenode.net'
      - name: from
        in: query
        description: date from in YYYYMMDD format
        required: false
        type: string
        example: '2019-01-14'
      - name: to
        in: query
        description: end date of interval to search in YYYYMMDD format
        required: false
        type: string
        example: '2019-09-23'
      - name: time
        in: query
        description: exact timestamp
        required: false
        type: string
        example: '2019-01-14 08:00:00 UTC'
      - name: message
        in: query
        description: exact message string
        example: 'msfloss: what a great bot!'
      - name: by
        in: query
        schema:
          type: string
          enum:
            - time
            - date
          default: date
        description: how to bin message counts
        example: 'time'
      responses:
        200:
          description: successfull operation
          schema:
            type: string
            example: '[ 2019-05-20: 10, 2019-05-21: 5, 2019-06-02: 23 ]'

  /register:
    post:
      tags:
      - register
      summary: Add new server/channel
      description: ""
      operationId: register
      consumes:
      - application/json
      parameters:
      - in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/RegisterRequest'
      responses:
        200:
          description: Success

  /unregister:
    post:
      tags:
      - unregister
      summary: Unregister channel
      operationId: unregister
      consumes:
      - application/json
      produces:
      - application/json
      parameters:
      - in: body
        name: body
        required: true
        schema:
          $ref: '#/definitions/RegisterRequest'
      responses:
        200:
          description: successful operation
definitions:
  Message:
    type: object
    properties:
      _id:
        type: string
      nick:
        type: string
      message:
        type: string
      time:
        type: string
        format: date-time
      channel:
        type: string
        example: '#flusp'
      server:
        type: string
  RegisterRequest:
    type: object
    properties:
      server:
        type: string
      channel:
        type: string
