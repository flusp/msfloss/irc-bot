# frozen_string_literal: true

require 'json'
require 'sinatra/base'

require_relative 'observe_chat/version'
require_relative 'settings'
require_relative 'observe_bot'

# Namespace for ObserveChat
module ObserveChat
  class Error < StandardError; end

  trap 'INT' do
    Thread.list.each do |t|
      t.exit unless t == Thread.current
      exit
    end
  end

  # ObserveChat
  # Application that handles bots.
  class ObserveChat < Sinatra::Base
    # Mutex for bot threads.
    @@sem = Mutex.new

    def self.bots
      @@bots
    end

    def self.bots_t
      @@bots_t
    end

    def self.options
      @@options
    end

    def load_setting(path = './observe_chat.yml')
      @@options = Settings.parse(path)
    end

    def configure_bot
      server = @@options.irc.first[0]
      channels = @@options.irc.first[1]

      @@bots = [new_bot(server, channels)]
      @@bots_t = []
    end

    def new_bot(server, channels)
      [ObserveBot.new(server, channels, @@options.settings('bot_name'),
                      @@options.database), server]
    end

    def register_bot(server, channels)
      b = new_bot(server, channels)
      @@sem.synchronize do
        @@bots.append(b)
        @@bots_t.append(Thread.new { b[0].start })
      end
    end

    def find_bot(server)
      @@sem.synchronize { @@bots.detect { |b| b[1] == server } }[0]
    end

    def self.find_idle
      @@bots.find_index do |b|
        f = b[0]
        f.nil? || f.irc.nil? || !f.irc.registered? || f.channels.empty?
      end
    end

    def self.kill_bot(idx)
      @@bots.delete_at(idx)
      Thread.kill(@@bots_t[idx])
      @@bots_t.delete_at(idx)
    end

    def self.extract_json(req)
      json = JSON.parse(req.body.read)
      server = json['server']
      channel = json['channel']
      pw = json.key?('password') ? json['password'] : nil
      [server, channel, pw]
    end

    def self.drop_idles
      @@sem.synchronize do
        loop do
          i = ObserveChat.find_idle
          break if i.nil?

          b = @@bots[i]
          @@options.drop_server(b[1])
          b[0]&.quit
          ObserveChat.kill_bot(i)
        end
      end
    end

    def start_observer
      @@sem.synchronize do
        @@bots.each { |b| @@bots_t.append(Thread.new { b[0].start }) }
      end
      janitor = Thread.new do
        sleep(1000)
        puts('Dropping idle bots...')
        ObserveChat.drop_idles
      end
      ObserveChat.run!
      @@bots_t.each(&:join)
      janitor.join
    end

    def db
      # Every bot points to the same database.
      ObserveChat.bots.empty? ? nil : ObserveChat.bots[0][0]
    end

    # REST API

    get '/' do
      File.read(File.join('_build/html', 'api_doc.html'))
    end

    get '/activity' do
      a = db&.find(params)
      by = params['by'].nil? ? 'date' : params['by']
      a.map! do |e|
        v = e[:time]
        by == 'time' ? v.to_time.hour : v.to_date
      end
      a.each_with_object(Hash.new(0)) { |e, t| t[e] += 1 }.to_json
    end

    get '/count' do
      content_type :json
      if params['channel'] then params['channel'] = '#' + params['channel'] end
      db&.count(params).to_s
    end

    get '/resources' do
      JSON.pretty_generate(YAML.safe_load(File.read('./swagger.yaml')))
    end

    get '/messages' do
      content_type :json
      if params['channel'] then params['channel'] = '#' + params['channel'] end
      db&.find(params).to_json
    end

    post '/register' do
      content_type :json
      server, channel, pw = ObserveChat.extract_json(request)
      s, c = ObserveChat.options.register_channel(server, channel)
      if !s
        register_bot(server, [channel])
        return "Registered to #{server}:  #{channel}"
      elsif !c
        find_bot(server).join(channel, pw)
        return "Registered to #{channel}"
      end
      'Already registered in the given server and channel.'
    end

    get '/registered' do
      content_type :json
      s = ObserveChat.options.irc
      v = s.values_at(*params.keys)
      v.empty? ? s.to_s : v.to_s
    end

    post '/unregister' do
      content_type :json
      server, channel, = ObserveChat.extract_json(request)
      if ObserveChat.options.drop_channel(server, channel)
        find_bot(server).part(channel)
        ObserveChat.drop_idles
        return 'Unregistered successfully.'
      end
      'Unregister failed.'
    end
  end
end
