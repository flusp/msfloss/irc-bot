# frozen_string_literal: true

module ObserverChat
  # Message
  # Represents an IRC mock-up message.
  #
  # IMPORTANT:
  # This class should not be used for actually storing data. Only for testing!
  class Message
    attr_reader :user, :message, :time, :channel, :server

    # Mock of Cinch::User
    class User
      attr_reader :nick

      def initialize(nick)
        @nick = nick
      end
    end

    # Mock of Cinch::Channel
    class Channel
      def initialize(channel)
        @channel = channel
      end

      def to_s
        @channel
      end
    end

    def initialize(user, message, time, channel, server)
      @user = User.new(user)
      @message = message
      @time = time
      @channel = Channel.new(channel)
      @server = server
    end
  end
end
