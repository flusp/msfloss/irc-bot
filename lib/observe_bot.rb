# frozen_string_literal: true

require 'cinch'
require_relative '../database/database_wrapper'

module ObserveChat
  # ObserveBot
  # The bot itself.
  class ObserveBot < Cinch::Bot
    def initialize(server, channels, bot_name, db_cfg)
      super()
      @db = DatabaseWrapper.new(db_cfg)

      @config.server = server
      @config.channels = channels
      @config.nick = bot_name
      @config.user = bot_name
      @config.realname = bot_name

      db_instance = @db
      on :channel do |msg|
        db_instance.save_message(msg, server)
      end
    end

    def count(params)
      @db.count(params)
    end

    def find(params)
      @db.find(params)
    end
  end
end
