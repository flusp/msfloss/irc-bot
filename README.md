# Introduction
IRC Bot is a customized system based on Cinch framework. It allows you to start 
bots to capture data from different channels and servers in IRC - Internet Relay
Chat. The bot observe all activity within a chat room and persists data in a 
database, and make this data available via REST API. This document aims to show 
users how to install and use the bot as well as to make available the main 
features of each important class in this micro-system. 

For more details please check out the [Wiki](https://gitlab.com/flusp/msfloss/irc-bot/wikis/home)

# How to Install
First, set the environment with the following steps:
- Install RVM version 2.6.1.
- Install Bundle version 2.0.1.
- Install MongoDB.

Afterwords, run

	$ bundle install

Now, have MongoDB running and you’re ready to use the bot.

# How to Use

- Use the following command to start the bot

        $ bin/observe_chat -s --setting examples/observe_chat.yml

The file observe_chat.yml is where you define your parameters such as server, channel etc.

- You may then access the REST API:

        $ curl address:port/messages?nick=mrflusp&channel=%23%23linux

- The table below shows all the available options of observe_chat:

| Options              	|         Definition         	|
|----------------------	|:--------------------------:	|
| -s, --start          	|     Start observe chats    	|
|     --setting [PATH] 	|        Load settings       	|
| -h, --help           	| Show all available options 	|
|     --version        	| Show version               	|

## Documentation

You can check the API's doc by accessing the bot's `address:port` with
your browser. The API should automatically generate the HTML docs and
display it on your browser.
